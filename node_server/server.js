var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var path = require('path');
var mysql = require('mysql');

var connection = mysql.createConnection({
    host: 'localhost',
    user: 'project_user',
    password: 'Wu-tang36',
    database: 'project'
});

var app = express();

connection.connect();


app.use(bodyParser.json());
app.use(cors());
app.use(express.static(path.join(__dirname+"/")));

app.get('/', function(request, response){
    response.send('Hello i am api');
});


var user_id=1;
app.get('/getSaves', function(req, res){
    connection.query('select * from saves where Users_id = '+ user_id + ';',function (err, rows, fields) {
        res.send(rows);
    });
});
app.get('/getCatExpence', function(req, res){
    connection.query('select * from catexp where Users_id = '+ user_id + ';',function (err, rows, fields) {
        res.send(rows);
    });
});
app.get('/getUser', function(req, res){
    connection.query('select * from users where id = '+ user_id + ';',function (err, rows, fields) {
        console.log(rows)
        res.send(rows);
    });
});
app.get('/getIncomes', function(req, res){
    connection.query('select * from incomes where Users_id = '+ user_id + ';',function (err, rows, fields) {
        res.send(rows);
    });
});
app.get('/getExpence', function(req, res){
    connection.query('select * from _exp where Users_id = '+ user_id + ';',function (err, rows, fields) {
        res.send(rows);
    });
});
app.get('/getSource', function(req, res){
    connection.query('select * from sources where Users_id = '+ user_id + ';',function (err, rows, fields) {
        res.send(rows);
    });
});
app.post('/addIncome', function(req, res){
   var item = req.body; 
    console.log(item)
    connection.query("insert into incomes (_data, Sources_id, comments, Users_id, _sum) values ('"+ item.date +"','"+  item.sourceId +"','"+  item.comments +"','"+  user_id +"','"+ item._sum+"');",   function (err, rows, fields) {
    });
    connection.query("update saves set balans = balans + "+ item._sum + " where id = "+item.save_id+ ";", function(req,res){
        
    });
    res.send(true);
});
app.post('/addExpence', function(req, res){
   var item = req.body; 
    connection.query("insert into _exp (_data, Catexp_id, comments, Users_id, _sum) values ('"+ item.date +"','"+  item.catExpId +"','"+  item.comments +"','"+  user_id +"','"+ item._sum+"');",   function (err, rows, fields) {
    });
    connection.query("update saves set balans = balans - "+ item._sum + " where id = "+item.save_id+ ";", function(req,res){
        
    });
    res.send(true);
});
app.post('/addSave', function(req, res){
   var item = req.body; 
   connection.query("insert into saves (_name, Users_id, balans, data_create) values ('"+ item._name +"','"+  user_id +"','"+  item.balans +"','"+  item.date_created +"');",   function (err, rows, fields) {
    });
    res.send(true);
});
app.post('/addSource', function(req, res){
   var item = req.body; 
   connection.query("insert into sources (sources_name, Users_id) values ('"+ item.sources_name +"','"+  user_id +"');",   function (err, rows, fields) {
    });
    res.send(true);
});
app.post('/addCatExpence', function(req, res){
   var item = req.body; 
   connection.query("insert into catexp (_name, Users_id) values ('"+ item._name +"','"+  user_id +"');",   function (err, rows, fields) {
    }); 
    res.send(true);
});
app.delete('/removeSave/:id',function(req,res){
    var id = req.params.id;
    connection.query("delete from saves where Users_id ="+user_id+" and id ="+id+";",   function (err, rows, fields) {
    }); 
     res.send(true)
});
app.delete('/removeSource/:id',function(req,res){
    var id = req.params.id;
    connection.query("delete from sources where Users_id ="+user_id+" and id ="+id+";",   function (err, rows, fields) {
    }); 
     res.send(true)
});
app.delete('/removeCatExpence/:id',function(req,res){
    var id = req.params.id;
    connection.query("delete from _exp where Users_id = "+user_id+" and Catexp_id ="+id+";",   function (err, rows, fields) {
    });
     connection.query("delete from catexp where Users_id = "+user_id+" and id ="+id+";",   function (err, rows, fields) {
    }); 
     res.send(true)
});
app.post('/changeSave',function(req,res){
    var item = req.body;
    connection.query("update sources set sources_name ='"+ item._name+"' where Users_id = "+user_id+" and id = "+item.id+";",   function (err, rows, fields) {
    }); 
});
app.post('/changeCatExpence',function(req,res){
    var item = req.body;
    connection.query("update catexp set _name ='"+ item._name+"' where Users_id = "+user_id+" and id = "+item.id+";",   function (err, rows, fields) {
    }); 
});
app.post('/changeSource',function(req,res){
    var item = req.body;
    connection.query("update sources set sources_name ='"+ item.sources_name+"' where Users_id = "+user_id+" and id = "+item.id+";",   function (err, rows, fields) {
    }); 
});

app.post('/login', function(req, res){
    var item = req.body;

    connection.query('select id, _name, surname from users where login = '+'"'+ item.login +'"'+'and pass ='+ '"'+ item.password +'"'+';',function (err, rows, fields) {
        for (var i in rows){
            user_id=rows[i].id
        }
        res.send(rows);
    });
    
});

app.post('/registration', function(req, res){
    var item = req.body;
   connection.query("insert into users (_name, surname, login, pass) values ('"+ item.name +"','"+  item.sname +"','"+  item.login +"','"+  item.password +"');",   function (err, rows, fields) {
   });

    var obj = Object.assign({},item);
     delete obj.password;
    res.send(obj);
           
});
app.post('/logout', function(req, res){
  user_id=0;
//    console.log(user_id)
    res.send(true)
});

app.listen(8080, function(){
    console.log('listen on port')
});