app.controller('acountCtrl', ['$scope','UserService', function($scope,UserService) {
     var vm=this;
     vm.user=[];
    vm.saves=[];
    vm.sources=[];
    vm.catExpence=[];
    vm.change_save={};
    vm.change_source={};
    vm.change_catExpence={};
    console.table(vm.saves)
    
    vm.add_save=function(){
        vm.obj={
            _name:vm.add_save_name,
            date_created: $scope.date
        }
        UserService.addSave(vm.obj)
        vm.add_save_name='';
    }
    vm.add_source=function(){
        vm.obj={
            sources_name:vm.add_source_name,
            date_created: $scope.date
        }
//        console.log(vm.obj);
        UserService.addSource(vm.obj)
        vm.add_source_name='';
    }
    vm.add_catExpence=function(){
        vm.obj={
            _name:vm.add_catExpence_name,
            date_created: $scope.date
        }
        UserService.addCatExpence(vm.obj)
        vm.add_catExpence_name='';
    }
    vm.remove_save=function(id){
        UserService.removeSave(id)
    }
    vm.remove_source=function(id){
//        console.log(id)
        UserService.removeSource(id)
    }
    vm.remove_catExpence=function(id){
        UserService.removeCatExpence(id)
    }
    vm.edit_save=function(item){
         vm.change_save = item;
    }
    vm.edit_source=function(item){
         vm.change_source = item;
        console.log(vm.change_source)
    }
     vm.change_save_func = function(){
             UserService.changeSave(vm.change_save)     
    }
     vm.change_source_func = function(){
//         console.log(vm.change_source)
             UserService.changeSource(vm.change_source)     
    }
     vm.edit_catExpence=function(item){
         vm.change_catExpence = item;
    }
     vm.change_catExpence_func = function(){
             UserService.changeCatExpence(vm.change_catExpence)     
    }
     
    vm.curUser={};
    
     vm.init=function(){
         vm.user=UserService.getUser(function(data){
            vm.user = data;
        }); 
          vm.saves=UserService.getSaves(function(data){
            vm.saves = data;
        });
         vm.catExpence=UserService.getCatExpence(function(data){
            vm.catExpence = data;
        });
         vm.sources=UserService.getSources(function(data){
            vm.sources = data;
        });
         vm.curUser=UserService.getCurUser();
    }
    vm.init()
   
}]);