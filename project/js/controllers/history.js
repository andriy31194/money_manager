app.controller('historyCtrl', ['$scope','UserService', function($scope,UserService) {
     var vm=this;
     vm.saves=[];
    vm.catExpence=[];
    vm.sources=[];
    vm.incomes=[];
    vm.expence=[];
    vm.monthIncomes=[];
    vm.monthExpence=[];
    vm.month=[
        {
            id:2,
            name:'february'
        },
        {
            id:3,
            name:'march'
        },
        {
            id:4,
            name:'april'
        },
        {
            id:5,
            name:'may'
        },
        {
            id:6,
            name:'june'
        },
        {
            id:7,
            name:'july'
        },
        {
            id:8,
            name:'august'
        },
        {
            id:9,
            name:'september'
        },
        {
            id:10,
            name:'october'
        },
        {
            id:11,
            name:'november'
        },
        {
            id:12,
            name:'december'
        },
        {
            id:1,
            name:'january'
        }
    ];
    
    vm.getMonthNumber=function(id){
        UserService.setMonthIncomes(id);
        vm.monthIncomes=UserService.getMonthIncomes();
        vm.monthExpence=UserService.getMonthExpence();
    }
    
    
    
     vm.init=function(){
        vm.saves=UserService.getSaves(function(data){
            vm.saves = data;
        });
         vm.catExpence=UserService.getCatExpence(function(data){
            vm.catExpence = data;
        });
         vm.sources=UserService.getSources(function(data){
            vm.sources = data;
        });   
    }
    vm.init()
   
}]);