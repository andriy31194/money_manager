app.controller('registrationCtrl', ['UserService', function(UserService) {
     var vm=this;
     vm.header = angular.element(document.querySelector(".header"));
    vm.header.css("display", "none");
    
    vm.register=function(){
        if(vm.firstName == '' && vm.lastName == '' && vm.username == '' && vm.password == ''){
            return
        }
      vm.user = {
          name:vm.firstName,
          sname:vm.lastName,
          login:vm.username,
          password:vm.password
      }
//      console.log(vm.user);
      vm.firstName='';
      vm.lastName='';
      vm.username='';
      vm.password='';
        UserService.registration(vm.user)
    }
    
     vm.init=function(){
          
    }
    vm.init()
   
}]);