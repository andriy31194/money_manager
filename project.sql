drop database if exists project;
CREATE database if not exists project DEFAULT CHARACTER SET utf8;
set SQL_SAFE_UPDATES = 0;
use project;

create table Users (
id int primary key auto_increment NOT NULL,
_name varchar (255) NOT NULL,
surname varchar (255) NOT NULL,
login varchar (255),
pass varchar (255),
photo blob
);

create table Saves(
id int primary key auto_increment NOT NULL,
_name varchar (255) NOT NULL,
balans float,
data_create varchar (255),
Users_id int
);

create table Sources (
id int primary key auto_increment NOT NULL,
sources_name varchar (255) NOT NULL,
Users_id int
);

create table Catexp (
id int primary key auto_increment NOT NULL,
_name varchar (255) NOT NULL,
Users_id int
);

create table _exp (
id int primary key auto_increment NOT NULL,
_data varchar (255),
_sum float,
Catexp_id int,
Users_id int,
comments varchar (255)
);

create table Incomes (
id int primary key auto_increment NOT NULL,
_data varchar (255),
Sources_id int,
comments varchar (255),
Users_id int,
_sum float
);

alter table Saves add constraint fk_Users_Saves foreign key 
(Users_id) references Users(id);

alter table Sources add constraint fk_Users_Sources foreign key 
(Users_id) references Users(id);

alter table Catexp add constraint fk_Users_Catexp foreign key 
(Users_id) references Users(id);

alter table _exp add constraint fk_Catexp_exp foreign key 
(Catexp_id) references Catexp(id);

alter table _exp add constraint fk_Users_exp foreign key 
(Users_id) references Users(id);

alter table Incomes add constraint fk_Sources_Incomes foreign key 
(Sources_id) references Sources(id);

alter table Incomes add constraint fk_Users_Incomes foreign key 
(Users_id) references Users(id);

insert into Users(_name, surname, login, pass) values ('Tom', 'Riddle', 'tom_riddle', 12345),('Jack', 'Riddle', 'jack_riddle', 23451);
insert into Saves(_name, data_create, balans, Users_id) values ('cache', '2017-11-25', 2500, 1),('visa', '2017-11-25', 5000, 2);

insert into Sources(sources_name, Users_id) values ('salary', 1), ('frrelance', 2), ('gift', 1), ('percentages', 2), ('salary', 2 ), ('freelance', 1 );

insert into Catexp(_name, Users_id) values ('food', 1), ('cars', 2), ('home', 1 ), ('caffe', 2 );

insert into _exp(_data, Catexp_id, Users_id, _sum, comments) values ('2017-11-25', 1, 1, '500', 'some commnets'), ('2017-11-26', 2, 2, '700', 'some commnets'), ('2017-11-26', 3, 1, '700', 'some commnets'), ('2017-11-26', 4, 2, '700', 'some commnets'), ('2017-11-26', 4, 1, '700', 'some commnets'), ('2017-11-26', 3, 1, '700', 'some commnets');

insert into Incomes(_data, Sources_id, comments, Users_id, _sum) values ('2017-11-25', 1, 'some comments', 1, '1000'), ('2017-11-27', 2, 'some commnets', 2, '300'), ('2017-11-27', 1, 'some commnets', 1, '300'), ('2017-11-27', 2, 'some commnets', 2, '300'), ('2017-11-27', 1, 'some commnets', 1, '300'), ('2017-11-27', 2, 'some commnets', 2, '300');

SELECT * FROM Catexp;